# Builder
FROM golang:1.20-alpine AS builder

RUN apk --no-cache add tzdata

RUN mkdir /app

COPY . /app

WORKDIR /app

RUN go mod download

RUN go clean -modcache

RUN apk update && apk add bash make cmake

RUN go install github.com/swaggo/swag/cmd/swag@latest

RUN swag init --parseInternal --parseDepth 1 -g cmd/main.go

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo -o boilerplate_repository ./cmd

# Run the tests in the container
FROM builder AS test-stage

RUN go test -v ./...

# Final Stage
FROM alpine:latest AS final-stage

WORKDIR /

# Set Time Zone
COPY --from=test-stage /usr/share/zoneinfo/Asia/Jakarta /etc/localtime

COPY --from=test-stage /app/boilerplate_repository ./

EXPOSE 3000

ENTRYPOINT [ "./boilerplate_repository" ]