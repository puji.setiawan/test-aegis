# include .env

.PHONY: run
# make run
run:
	@-echo "🚀 Run app..."
	@go run cmd/main.go

.PHONY: migrate-up migrate-down migrate-create
# make migrate-up
migrate-up:
	@-echo "⬆️ Migrating up..."
	@dbmate --migrations-dir ./database/migrations -u "postgresql://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=${DB_SSL_MODE}" -wait up
	@-echo "✅ Migrating up is success!"

# make migrate-down
# make migrate-down version=1
migrate-down:
	@-echo "⬇️ Migrating down..."
	@dbmate --migrations-dir ./database/migrations -u "postgresql://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=${DB_SSL_MODE}" -wait down
	@-echo "✅ Migrating down is success!"

# make migrate-create name="create_users_table"
migrate-create:
	@-echo "🪄 Creating migration file..."
	@dbmate --migrations-dir ./database/migrations new $(name)
	@-echo "✅ Migration file successfully created!"

.PHONY: test-run
# make test-run
test-run:
	@-echo "🧪 Running all test..."
	@go test ./test/app/...
	@go test ./test/config/...
	@go test ./test/database/...
	@go test ./test/pkg/...
	@-echo "✅ All test run successfully!"

.PHONY: format
# make format
format:
	@-echo "🏗️ Formatting projects code..."
	@gofmt -w . && goimports -w .
	@-echo "✅ Codes formatted successfully!"

.PHONY: security-check
security-check:
	@-echo "️‍👮‍ Run go security check..."
	@gosec ./...
	@-echo "👌All good!"
	@-echo "✅ Security check finished!"

.PHONY: lint
lint:
	@-echo "🔎 Run go linter..."
	@go vet ./...
	@golangci-lint run ./...
	@-echo "👌 All good!"
	@-echo "✅ Linter finished running!"

.PHONY: swag
swag:
	@swag init --parseInternal --parseDepth 1 -g cmd/main.go
