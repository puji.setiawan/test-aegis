package middleware

import (
	apphtpp "boilerplate_repository/pkg/http"
	"fmt"
	"os"
	"runtime/debug"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

// GinRecoveryMiddleware is a middleware for Gin that captures panics and recovers from them
func GinRecoveryMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			rcv := panicRecover(recover())
			if rcv != nil {
				fmt.Fprintf(os.Stderr, "Panic: %+v\n", rcv)
				debug.PrintStack()
				apphtpp.ResponseInternalServerError(c, rcv)
			}
		}()

		c.Next()
	}
}

func recovery(r any) error {
	var err error
	if r != nil {
		switch t := r.(type) {
		case string:
			err = errors.New(t)
		case error:
			err = t
		default:
			err = errors.New("Unknown error")
		}
	}
	return err
}

func panicRecover(rc any) error {
	return recovery(rc)
}
