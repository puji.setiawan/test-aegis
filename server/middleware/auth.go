package middleware

import (
	"boilerplate_repository/pkg/http"
	"boilerplate_repository/pkg/jwt"
	"boilerplate_repository/pkg/messege"
	usercontext "boilerplate_repository/pkg/user_context"
	"strings"

	apperr "boilerplate_repository/pkg/errors"

	"github.com/pkg/errors"

	"github.com/gin-gonic/gin"
)

type Auth struct {
	jwt *jwt.Jwt
}

func NewAuth(jwt *jwt.Jwt) *Auth {
	return &Auth{
		jwt: jwt,
	}
}

const fnAuthenticate = "Authenticate"

func (auth *Auth) Authenticate() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authToken := ctx.GetHeader("Authorization")
		if authToken == "" {
			http.ResponseUnauthorized(ctx, apperr.MiddlewareError(
				fnAuthenticate,
				"ctx.GetHeader",
				errors.New("token is required"),
			))
			ctx.Abort()
			return
		}

		token, err := auth.checkValidTokenType(authToken)
		if err != nil {
			http.ResponseUnauthorized(ctx, apperr.MiddlewareError(
				fnAuthenticate,
				"auth.checkValidTokenType",
				err,
			))
			ctx.Abort()
			return
		}

		userData, err := auth.jwt.ValidateAccessToken(token)
		if err != nil {
			http.ResponseUnauthorized(ctx, apperr.MiddlewareError(
				fnAuthenticate,
				"auth.jwt.ValidateAccessToken",
				err,
			))
			ctx.Abort()
			return
		}

		ctx.Set(string(usercontext.UserDataCtxKey), userData)
		ctx.Next()
	}
}

func (auth *Auth) checkValidTokenType(token string) (string, error) {
	strArr := strings.Split(token, " ")
	if len(strArr) != 2 || strings.ToLower(strArr[0]) != "bearer" {
		return "", messege.ErrTokenIsNotBearer
	}
	return strArr[1], nil
}
