package router

import (
	"boilerplate_repository/config"
	"boilerplate_repository/internal/handler"
	"boilerplate_repository/server/middleware"
	"log"

	"github.com/fvbock/endless"
	"github.com/gin-gonic/gin"
)

type Router struct {
	router       *gin.Engine
	handler      *handler.ExampleHandler
	middleware   *middleware.Auth
	cfg          *config.Config
	user         *handler.UserHandler
	organization *handler.OrganizationHandler
}

func NewRouter(
	router *gin.Engine,
	handler *handler.ExampleHandler,
	cfg *config.Config,
	middleware *middleware.Auth,
	user *handler.UserHandler,
	organization *handler.OrganizationHandler,
) *Router {
	return &Router{
		router:       router,
		handler:      handler,
		middleware:   middleware,
		cfg:          cfg,
		user:         user,
		organization: organization,
	}
}

func (r *Router) Run(port string) {
	r.setRoutes()
	r.run(port)
}

func (r *Router) setRoutes() {
	v1Router := r.router.Group("/api/v1")
	v1Router.Use(middleware.GinRecoveryMiddleware())

	r.setAuthRoutes(v1Router)
	r.setUserRoutes(v1Router)
	r.setOrgRoutes(v1Router)
}

// func (r *Router) setSwaggerRoutes() {
// 	r.router.GET("/swagger-ui/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
// }

func (r *Router) setAuthRoutes(rg *gin.RouterGroup) {
	authRoutes := rg.Group("/auth")
	authRoutes.POST("/login", r.handler.GetToken)
	// authRoutes.POST("/refresh", r.handler.GetRefreshToken)
}

func (r *Router) setUserRoutes(rg *gin.RouterGroup) {
	sakuAuthRoutes := rg.Group("/user")
	sakuAuthRoutes.Use(r.middleware.Authenticate())
	sakuAuthRoutes.GET("/:id", r.user.GetDetail)
	sakuAuthRoutes.DELETE("/:id", r.user.Delete)
	sakuAuthRoutes.POST("/", r.user.Create)
	sakuAuthRoutes.PUT("/:id", r.user.Update)
}

func (r *Router) setOrgRoutes(rg *gin.RouterGroup) {
	sakuAuthRoutes := rg.Group("/organization")
	sakuAuthRoutes.Use(r.middleware.Authenticate())
	sakuAuthRoutes.GET("/:id", r.organization.GetDetail)
	sakuAuthRoutes.DELETE("/:id", r.organization.Delete)
	sakuAuthRoutes.POST("/", r.organization.Create)
	sakuAuthRoutes.PUT("/:id", r.organization.Update)
}

func (r *Router) run(port string) {
	if port == "" {
		port = "8080"
	}
	log.Printf("running on port : [%v]", port)
	if err := endless.ListenAndServe(":"+port, r.router); err != nil {
		log.Fatalf("failed to run on port [::%v]", port)
	}
}
