package dependency

import (
	"boilerplate_repository/config"
	"boilerplate_repository/infra/database"
	"boilerplate_repository/internal/handler"
	"boilerplate_repository/internal/repository"
	"boilerplate_repository/pkg/jwt"
	"boilerplate_repository/pkg/validation"

	"boilerplate_repository/internal/service"
	"boilerplate_repository/server/middleware"
	"boilerplate_repository/server/router"

	"github.com/gin-gonic/gin"
)

func InitializeRouter(r *gin.Engine) *router.Router {
	validatorImpl := validation.NewValidator()
	cfg := config.Get()
	jwt := jwt.New(cfg)

	// client add here
	db := database.New(cfg)
	middlewareAuth := middleware.NewAuth(jwt)

	// repo add here
	userRepo := repository.NewUserRepository(db)
	orgRepo := repository.NewOrganizationRepository(db)

	// service add here
	exampleService := service.NewExampleService(
		jwt,
		userRepo,
	)
	userService := service.NewUserService(
		userRepo,
		jwt,
	)
	orgService := service.NewOrganizationService(
		orgRepo,
		jwt,
	)

	// handler add here
	exampleHandler := handler.NewExampleHandler(
		validatorImpl,
		exampleService,
	)
	userHandler := handler.NewUserHandler(
		validatorImpl,
		userService,
	)
	orgHandler := handler.NewOrganizationHandler(
		validatorImpl,
		orgService,
	)

	// only 1 router
	routerRouter := router.NewRouter(
		r,
		exampleHandler,
		cfg,
		middlewareAuth,
		userHandler,
		orgHandler,
	)

	return routerRouter
}
