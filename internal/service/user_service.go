package service

import (
	"boilerplate_repository/internal/entity"
	"boilerplate_repository/internal/repository"
	"boilerplate_repository/internal/repository/models.go"
	apperr "boilerplate_repository/pkg/errors"
	"boilerplate_repository/pkg/jwt"
	"boilerplate_repository/pkg/messege"
	"boilerplate_repository/pkg/utils"
	"context"
	"database/sql"
	"errors"
	"time"
)

type UserService interface {
	Create(ctx context.Context, req *entity.UserReq) error
	GetDetail(ctx context.Context, id int) (*entity.UserRes, error)
	Update(ctx context.Context, req *entity.UserReq, id int) error
	Delete(ctx context.Context, id int) error
}

type UserServiceImpl struct {
	UserRepo repository.UserRepository
	Jwt      *jwt.Jwt
}

func NewUserService(
	UserRepository repository.UserRepository,
	Jwt *jwt.Jwt,
) UserService {
	return &UserServiceImpl{
		UserRepo: UserRepository,
		Jwt:      Jwt,
	}
}

const (
	fnCreateUser    = "CreateUser"
	fnUpdateUser    = "UpdateUser"
	fnGetDetailUser = "GetDetailUser"
	fnDeleteUser    = "DeleteUser"
)

func (s *UserServiceImpl) Create(ctx context.Context, req *entity.UserReq) error {
	// userId := usercontext.GetUserIdFromContext(ctx)

	err := s.UserRepo.Create(ctx, models.UserSchema{
		Name:      req.Nama,
		Role:      "user",
		Password:  req.Password,
		CreatedAt: time.Now(),
	})
	if err != nil {
		return apperr.ServiceError(fnCreateUser, "s.UserRepo.Create", apperr.TypeInternalServerError, err)
	}

	return nil
}

func (s *UserServiceImpl) GetDetail(ctx context.Context, id int) (*entity.UserRes, error) {
	user, err := s.UserRepo.GetDetailUser(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, apperr.ServiceError(fnGetDetailUser, "ExampleRepo.GetExample", apperr.TypeNotFound, messege.ErrUserNotFound)
		}
		return nil, apperr.ServiceError(fnGetDetailUser, "UserClient.GetPokemon", apperr.TypeInternalServerError, err)
	}
	return &entity.UserRes{
		Nama:      user.Name,
		Role:      user.Role,
		CreatedAt: utils.FormatDate(user.CreatedAt),
		UpdatedAt: utils.FormatDate(user.UpdatedAt),
		DeletedAt: utils.FormatDate(user.DeletedAt),
	}, nil
}

func (s *UserServiceImpl) Update(ctx context.Context, req *entity.UserReq, id int) error {
	user, err := s.UserRepo.GetDetailUser(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return apperr.ServiceError(fnGetDetailUser, "ExampleRepo.GetExample", apperr.TypeNotFound, messege.ErrUserNotFound)
		}
		return apperr.ServiceError(fnGetDetailUser, "UserClient.GetPokemon", apperr.TypeInternalServerError, err)
	}

	user.Name = req.Nama
	user.Password = req.Password

	err = s.UserRepo.Update(ctx, user, id)
	if err != nil {
		return apperr.ServiceError(fnGetDetailUser, "UserClient.GetPokemon", apperr.TypeInternalServerError, err)
	}

	return nil
}

func (s *UserServiceImpl) Delete(ctx context.Context, id int) error {
	_, err := s.UserRepo.GetDetailUser(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return apperr.ServiceError(fnGetDetailUser, "ExampleRepo.GetExample", apperr.TypeNotFound, messege.ErrUserNotFound)
		}
		return apperr.ServiceError(fnGetDetailUser, "UserClient.GetPokemon", apperr.TypeInternalServerError, err)
	}

	err = s.UserRepo.Delete(ctx, id)
	if err != nil {
		return apperr.ServiceError(fnGetDetailUser, "UserClient.GetPokemon", apperr.TypeInternalServerError, err)
	}

	return nil
}
