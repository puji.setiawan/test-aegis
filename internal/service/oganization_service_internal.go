package service

import (
	usercontext "boilerplate_repository/pkg/user_context"
	"errors"
)

func (s *OrganizationServiceImpl) validate(uctx *usercontext.UserData, createdBy int) error {
	if uctx.Role == "user" {
		if uctx.ID != createdBy {
			return errors.New("forbiddenn")
		}
	}

	return nil
}
