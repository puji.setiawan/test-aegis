package service

import (
	"boilerplate_repository/internal/entity"
	"boilerplate_repository/internal/repository"
	"boilerplate_repository/internal/repository/models.go"
	apperr "boilerplate_repository/pkg/errors"
	"boilerplate_repository/pkg/jwt"
	"boilerplate_repository/pkg/messege"
	usercontext "boilerplate_repository/pkg/user_context"
	"boilerplate_repository/pkg/utils"
	"context"
	"database/sql"
	"errors"
	"time"
)

type OrganizationService interface {
	Create(ctx context.Context, req *entity.OrganizationReq) error
	GetDetail(ctx context.Context, id int) (*entity.OrganizationRes, error)
	Update(ctx context.Context, req *entity.OrganizationReq, id int) error
	Delete(ctx context.Context, id int) error
}

type OrganizationServiceImpl struct {
	OrganizationRepo repository.OrganizationRepository
	Jwt              *jwt.Jwt
}

func NewOrganizationService(
	OrganizationRepository repository.OrganizationRepository,
	Jwt *jwt.Jwt,
) OrganizationService {
	return &OrganizationServiceImpl{
		OrganizationRepo: OrganizationRepository,
		Jwt:              Jwt,
	}
}

const (
	fnCreateOrganization    = "CreateOrganization"
	fnUpdateOrganization    = "UpdateOrganization"
	fnGetDetailOrganization = "GetDetailOrganization"
	fnDeleteOrganization    = "DeleteOrganization"
)

func (s *OrganizationServiceImpl) Create(ctx context.Context, req *entity.OrganizationReq) error {
	uctx := usercontext.GetUserContext(ctx)

	err := s.OrganizationRepo.Create(ctx, models.OrganizationSchema{
		Name:      req.Nama,
		CreatedBy: uctx.ID,
		CreatedAt: time.Now(),
	})
	if err != nil {
		return apperr.ServiceError(fnCreateOrganization, "s.OrganizationRepo.Create", apperr.TypeInternalServerError, err)
	}

	return nil
}

func (s *OrganizationServiceImpl) GetDetail(ctx context.Context, id int) (*entity.OrganizationRes, error) {
	Organization, err := s.OrganizationRepo.GetDetailOrganization(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, apperr.ServiceError(fnGetDetailOrganization, "ExampleRepo.GetExample", apperr.TypeNotFound, messege.ErrOrganizationNotFound)
		}
		return nil, apperr.ServiceError(fnGetDetailOrganization, "OrganizationClient.GetPokemon", apperr.TypeInternalServerError, err)
	}
	return &entity.OrganizationRes{
		Nama:      Organization.Name,
		CreatedAt: utils.FormatDate(Organization.CreatedAt),
		UpdatedAt: utils.FormatDate(Organization.UpdatedAt),
		DeletedAt: utils.FormatDate(Organization.DeletedAt),
	}, nil
}

func (s *OrganizationServiceImpl) Update(ctx context.Context, req *entity.OrganizationReq, id int) error {
	Organization, err := s.OrganizationRepo.GetDetailOrganization(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return apperr.ServiceError(fnGetDetailOrganization, "ExampleRepo.GetExample", apperr.TypeNotFound, messege.ErrOrganizationNotFound)
		}
		return apperr.ServiceError(fnGetDetailOrganization, "OrganizationClient.GetPokemon", apperr.TypeInternalServerError, err)
	}

	uctx := usercontext.GetUserContext(ctx)
	err = s.validate(uctx, Organization.CreatedBy)
	if err != nil {
		return apperr.ServiceError(fnGetDetailOrganization, "OrganizationClient.GetPokemon", apperr.TypeForbidden, err)
	}

	Organization.Name = req.Nama

	err = s.OrganizationRepo.Update(ctx, Organization, id)
	if err != nil {
		return apperr.ServiceError(fnGetDetailOrganization, "OrganizationClient.GetPokemon", apperr.TypeInternalServerError, err)
	}

	return nil
}

func (s *OrganizationServiceImpl) Delete(ctx context.Context, id int) error {
	org, err := s.OrganizationRepo.GetDetailOrganization(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return apperr.ServiceError(fnGetDetailOrganization, "ExampleRepo.GetExample", apperr.TypeNotFound, messege.ErrOrganizationNotFound)
		}
		return apperr.ServiceError(fnGetDetailOrganization, "OrganizationClient.GetPokemon", apperr.TypeInternalServerError, err)
	}

	uctx := usercontext.GetUserContext(ctx)
	err = s.validate(uctx, org.CreatedBy)
	if err != nil {
		return apperr.ServiceError(fnGetDetailOrganization, "OrganizationClient.GetPokemon", apperr.TypeForbidden, err)
	}

	err = s.OrganizationRepo.Delete(ctx, id)
	if err != nil {
		return apperr.ServiceError(fnGetDetailOrganization, "OrganizationClient.GetPokemon", apperr.TypeInternalServerError, err)
	}

	return nil
}
