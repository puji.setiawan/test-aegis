package service

import (
	"boilerplate_repository/internal/entity"
	"boilerplate_repository/internal/repository"
	apperr "boilerplate_repository/pkg/errors"
	"boilerplate_repository/pkg/jwt"
	"boilerplate_repository/pkg/messege"
	"context"
	"strconv"
)

type ExampleService interface {
	Login(ctx context.Context, req *entity.LoginReq) (any, error)
}

type ExampleServiceImpl struct {
	Jwt      *jwt.Jwt
	UserRepo repository.UserRepository
}

func NewExampleService(
	Jwt *jwt.Jwt,
	UserRepo repository.UserRepository,
) ExampleService {
	return &ExampleServiceImpl{
		Jwt:      Jwt,
		UserRepo: UserRepo,
	}
}

const (
	fnGetExample       = "GetExample"
	fnGetPokmonExample = "GetPokmonExample"
	fnRefresh          = "Refresh"
)

func (s *ExampleServiceImpl) Login(ctx context.Context, req *entity.LoginReq) (any, error) {
	user, err := s.UserRepo.GetUserByUsername(ctx, req.Username)
	if err != nil {
		return nil, apperr.ServiceError(fnGetPokmonExample, "Jwt.GenerateToke", apperr.TypeInternalServerError, messege.ErrUserNotFound)
	}

	if req.Password != user.Password {
		return nil, apperr.ServiceError(fnGetPokmonExample, "Jwt.GenerateToke", apperr.TypeInternalServerError, messege.ErrInvalidPassword)
	}
	token, err := s.Jwt.GenerateToken(jwt.JwtRequest{
		UserId: strconv.Itoa(user.Id),
		Role:   user.Role,
	})
	if err != nil {
		return nil, apperr.ServiceError(fnGetPokmonExample, "Jwt.GenerateToke", apperr.TypeInternalServerError, err)
	}

	return token, nil
}
