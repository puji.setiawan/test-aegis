package entity

type (
	OrganizationReq struct {
		Nama string `json:"name"`
	}

	OrganizationRes struct {
		Nama      string `json:"name"`
		CreatedAt string `json:"CreatedAt"`
		UpdatedAt string `json:"UpdatedAt"`
		DeletedAt string `json:"DeletedAt"`
	}
)
