package entity

type (
	RefreshReq struct {
		Token string `json:"refreshToken" validate:"required"`
	}

	LoginReq struct {
		Username string `json:"username" validate:"required"`
		Password string `json:"password" validate:"required"`
	}
)
