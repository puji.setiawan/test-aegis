package entity

type (
	UserReq struct {
		Nama     string `json:"name"`
		Password string `json:"password"`
	}

	UserRes struct {
		Nama      string `json:"nama"`
		Role      string `json:"role"`
		CreatedAt string `json:"CreatedAt"`
		UpdatedAt string `json:"UpdatedAt"`
		DeletedAt string `json:"DeletedAt"`
	}
)
