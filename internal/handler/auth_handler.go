package handler

import (
	"boilerplate_repository/internal/entity"
	"boilerplate_repository/internal/service"
	"boilerplate_repository/pkg/errors"
	"boilerplate_repository/pkg/http"
	"boilerplate_repository/pkg/validation"

	"github.com/gin-gonic/gin"
)

type ExampleHandler struct {
	validator      validation.Validator
	ExampleService service.ExampleService
}

func NewExampleHandler(validator validation.Validator, ExampleService service.ExampleService) *ExampleHandler {
	return &ExampleHandler{validator, ExampleService}
}

func (h *ExampleHandler) GetToken(ctx *gin.Context) {
	req := new(entity.LoginReq)
	err := ctx.ShouldBindJSON(req)
	if err != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(err))
		return
	}
	validationErrs := h.validator.Validate(req)
	if validationErrs != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(validationErrs))
		return
	}

	res, err := h.ExampleService.Login(ctx, req)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, res)
}
