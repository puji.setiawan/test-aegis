package handler

import (
	"boilerplate_repository/internal/entity"
	"boilerplate_repository/internal/service"
	"boilerplate_repository/pkg/errors"
	"boilerplate_repository/pkg/http"
	"boilerplate_repository/pkg/utils"
	"boilerplate_repository/pkg/validation"

	"github.com/gin-gonic/gin"
)

type OrganizationHandler struct {
	validator           validation.Validator
	OrganizationService service.OrganizationService
}

func NewOrganizationHandler(validator validation.Validator, OrganizationService service.OrganizationService) *OrganizationHandler {
	return &OrganizationHandler{validator, OrganizationService}
}

func (h *OrganizationHandler) Create(ctx *gin.Context) {
	req := new(entity.OrganizationReq)
	err := ctx.ShouldBindJSON(req)
	if err != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(err))
		return
	}

	validationErrs := h.validator.Validate(req)
	if validationErrs != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(validationErrs))
		return
	}

	err = h.OrganizationService.Create(ctx, req)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, nil)
}

func (h *OrganizationHandler) Update(ctx *gin.Context) {
	id := ctx.Param("id")
	req := new(entity.OrganizationReq)
	err := ctx.ShouldBindJSON(req)
	if err != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(err))
		return
	}
	validationErrs := h.validator.Validate(req)
	if validationErrs != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(validationErrs))
		return
	}

	err = h.OrganizationService.Update(ctx, req, utils.ParseInt(id))
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, nil)
}

func (h *OrganizationHandler) Delete(ctx *gin.Context) {
	id := ctx.Param("id")
	err := h.OrganizationService.Delete(ctx, utils.ParseInt(id))
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, nil)
}

func (h *OrganizationHandler) GetDetail(ctx *gin.Context) {
	id := ctx.Param("id")
	res, err := h.OrganizationService.GetDetail(ctx, utils.ParseInt(id))
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, res)
}
