package handler

import (
	"boilerplate_repository/internal/entity"
	"boilerplate_repository/internal/service"
	"boilerplate_repository/pkg/errors"
	"boilerplate_repository/pkg/http"
	"boilerplate_repository/pkg/utils"
	"boilerplate_repository/pkg/validation"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	validator   validation.Validator
	UserService service.UserService
}

func NewUserHandler(validator validation.Validator, UserService service.UserService) *UserHandler {
	return &UserHandler{validator, UserService}
}

func (h *UserHandler) Create(ctx *gin.Context) {
	req := new(entity.UserReq)
	err := ctx.ShouldBindJSON(req)
	if err != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(err))
		return
	}

	validationErrs := h.validator.Validate(req)
	if validationErrs != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(validationErrs))
		return
	}

	err = h.UserService.Create(ctx, req)
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, nil)
}

func (h *UserHandler) Update(ctx *gin.Context) {
	id := ctx.Param("id")
	req := new(entity.UserReq)
	err := ctx.ShouldBindJSON(req)
	if err != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(err))
		return
	}
	validationErrs := h.validator.Validate(req)
	if validationErrs != nil {
		http.ResponseBadRequest(ctx, errors.HandlerError(validationErrs))
		return
	}

	err = h.UserService.Update(ctx, req, utils.ParseInt(id))
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, nil)
}

func (h *UserHandler) Delete(ctx *gin.Context) {
	id := ctx.Param("id")
	err := h.UserService.Delete(ctx, utils.ParseInt(id))
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, nil)
}

func (h *UserHandler) GetDetail(ctx *gin.Context) {
	id := ctx.Param("id")
	res, err := h.UserService.GetDetail(ctx, utils.ParseInt(id))
	if err != nil {
		http.ResponseError(ctx, errors.HandlerError(err))
		return
	}
	http.ResponseSuccess(ctx, res)
}
