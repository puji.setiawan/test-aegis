package repository

import (
	"boilerplate_repository/internal/repository/models.go"
	"context"

	"github.com/uptrace/bun"
)

type UserRepository interface {
	Create(ctx context.Context, user models.UserSchema) error
	GetDetailUser(ctx context.Context, id int) (*models.UserSchema, error)
	Update(ctx context.Context, user *models.UserSchema, id int) error
	Delete(ctx context.Context, id int) error
	GetUserByUsername(ctx context.Context, username string) (*models.UserSchema, error)
}

type UserRepositoryImpl struct {
	db *bun.DB
}

func NewUserRepository(db *bun.DB) *UserRepositoryImpl {
	return &UserRepositoryImpl{db}
}

func (r *UserRepositoryImpl) GetDetailUser(ctx context.Context, id int) (*models.UserSchema, error) {
	user := new(models.UserSchema)
	err := r.db.NewSelect().Model(user).Where(("id = ?"), id).Scan(ctx)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (r *UserRepositoryImpl) Create(ctx context.Context, user models.UserSchema) error {
	_, err := r.db.NewInsert().Model(&user).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *UserRepositoryImpl) Update(ctx context.Context, user *models.UserSchema, id int) error {
	_, err := r.db.NewUpdate().Model(user).Where(("id = ?"), id).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *UserRepositoryImpl) Delete(ctx context.Context, id int) error {
	_, err := r.db.NewDelete().Model(new(models.UserSchema)).Where(("id = ?"), id).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *UserRepositoryImpl) GetUserByUsername(ctx context.Context, username string) (*models.UserSchema, error) {
	user := new(models.UserSchema)
	err := r.db.NewSelect().Model(user).Where(("name = ?"), username).Scan(ctx)
	if err != nil {
		return nil, err
	}
	return user, nil
}
