package repository

import (
	"boilerplate_repository/internal/repository/models.go"
	"context"

	"github.com/uptrace/bun"
)

type OrganizationRepository interface {
	Create(ctx context.Context, Organization models.OrganizationSchema) error
	GetDetailOrganization(ctx context.Context, id int) (*models.OrganizationSchema, error)
	Update(ctx context.Context, Organization *models.OrganizationSchema, id int) error
	Delete(ctx context.Context, id int) error
}

type OrganizationRepositoryImpl struct {
	db *bun.DB
}

func NewOrganizationRepository(db *bun.DB) *OrganizationRepositoryImpl {
	return &OrganizationRepositoryImpl{db}
}

func (r *OrganizationRepositoryImpl) GetDetailOrganization(ctx context.Context, id int) (*models.OrganizationSchema, error) {
	Organization := new(models.OrganizationSchema)
	err := r.db.NewSelect().Model(Organization).Where(("id = ?"), id).Scan(ctx)
	if err != nil {
		return nil, err
	}
	return Organization, nil
}

func (r *OrganizationRepositoryImpl) Create(ctx context.Context, Organization models.OrganizationSchema) error {
	_, err := r.db.NewInsert().Model(&Organization).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *OrganizationRepositoryImpl) Update(ctx context.Context, Organization *models.OrganizationSchema, id int) error {
	_, err := r.db.NewUpdate().Model(Organization).Where(("id = ?"), id).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *OrganizationRepositoryImpl) Delete(ctx context.Context, id int) error {
	_, err := r.db.NewDelete().Model(new(models.OrganizationSchema)).Where(("id = ?"), id).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}
