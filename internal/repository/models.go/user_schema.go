package models

import (
	"time"

	"github.com/uptrace/bun"
)

type UserSchema struct {
	bun.BaseModel `bun:"users"`
	Id            int `bun:",autoincrement"`
	Name          string
	Role          string
	Password      string
	CreatedAt     time.Time `bun:",nullzero"`
	UpdatedAt     time.Time `bun:",nullzero"`
	DeletedAt     time.Time `bun:",soft_delete,nullzero"`
}
