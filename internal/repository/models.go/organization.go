package models

import (
	"time"

	"github.com/uptrace/bun"
)

type OrganizationSchema struct {
	bun.BaseModel `bun:"organization"`
	Id            int `bun:",autoincrement"`
	Name          string
	CreatedBy     int
	CreatedAt     time.Time `bun:",nullzero"`
	UpdatedAt     time.Time `bun:",nullzero"`
	DeletedAt     time.Time `bun:",soft_delete,nullzero"`
}
