package config

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Port        string `envconfig:"APP_PORT"`
	Environment string `envconfig:"APP_ENV"`

	// db
	DbUsername string `envconfig:"DB_USERNAME"`
	DbPassword string `envconfig:"DB_PASSWORD"`
	DbHost     string `envconfig:"DB_HOST"`
	DbPort     string `envconfig:"DB_PORT"`
	DbName     string `envconfig:"DB_NAME"`

	// redis
	RedisHost     string `envconfig:"REDIS_HOST"`
	RedisPort     string `envconfig:"REDIS_PORT"`
	RedisPassword string `envconfig:"REDIS_PASSWORD"`
	RedisDatabase int    `envconfig:"REDIS_DATABASE"`

	// jwt
	AccessTokenSecret    string        `envconfig:"ACCESS_TOKEN_SECRET"`
	RefreshTokenSecret   string        `envconfig:"REFRESH_TOKEN_SECRET"`
	AccessTokenDuration  time.Duration `envconfig:"ACCESS_TOKEN_DURATION"`
	RefreshTokenDuration time.Duration `envconfig:"REFRESH_TOKEN_DURATION"`

	// http
	HttpTimeout time.Duration `envconfig:"HTTP_TIMEOUT" default:"30s"`
	HttpDebug   bool          `envconfig:"HTTP_DEBUG" default:"false"`

	// client
	ExampleClientHost string `envconfig:"EXAMPLE_CLIENT_HOST"`
}

func Get() *Config {
	cfg := Config{}
	envconfig.MustProcess("", &cfg)
	return &cfg
}
