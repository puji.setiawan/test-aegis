package utils

import (
	"fmt"
	"strconv"
	"time"
)

func IndonesianFormat(t time.Time) string {
	day := strconv.Itoa(t.Day())
	month := toIndonesianMonth(t.Month())
	year := strconv.Itoa(t.Year())
	indonesianFormat := fmt.Sprintf("%s %s %s", day, month, year)
	return indonesianFormat
}

func toIndonesianMonth(month time.Month) string {
	indonesianMonth := map[time.Month]string{
		time.January:   "Januari",
		time.February:  "Februari",
		time.March:     "Maret",
		time.April:     "April",
		time.May:       "Mei",
		time.June:      "Juni",
		time.July:      "Juli",
		time.August:    "Agustus",
		time.September: "September",
		time.October:   "Oktober",
		time.November:  "November",
		time.December:  "Desember",
	}
	return indonesianMonth[month]
}

func FormatDate(t time.Time) string {
	return t.Format("02/01/2006")
}
