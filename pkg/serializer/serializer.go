package serializer

import (
	"encoding/base64"
	"github.com/vmihailenco/msgpack/v5"
	"log"
	"strings"
)

func SerializeForRedis(data interface{}) []byte {
	b, err := msgpack.Marshal(data)
	if err != nil {
		log.Println("Error encoding struct:", err)
		panic("Error encoding struct [SerializeForRedis]")
	}

	return b
}

func DeserializeForRedis(data string, result interface{}) error {
	if err := msgpack.Unmarshal([]byte(data), result); err != nil {
		log.Println("Error decoding struct:", err)
		panic("Error decoding struct [DeserializeForRedis]")
	}

	return nil
}

func Base64ToBytes(base64String string) ([]byte, error) {
	trimmedBase64String := strings.TrimPrefix(base64String, "data:image/jpeg;base64,")
	return base64.StdEncoding.DecodeString(trimmedBase64String)
}
