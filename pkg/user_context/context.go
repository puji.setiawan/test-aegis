package usercontext

import (
	"context"
)

type Key string

const UserDataCtxKey Key = "userContext"

type UserData struct {
	ID   int    `json:"id"`
	Role string `json:"role"`
}

func GetUserContext(ctx context.Context) *UserData {
	value, ok := ctx.Value(string(UserDataCtxKey)).(*UserData)
	if !ok {
		return nil
	}
	return value
}
