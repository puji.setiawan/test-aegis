package main

import "boilerplate_repository/server"

// @title EXAMPLE PROJECT API
// @version 0.1
// @description This service is to handle XXXXXXXXX. For more detail, please visit https://gitlab.com/XXXXXX
// @contact.name Saving Team
func main() {
	server.NewHttpServer().Run()
}
